    var express = require('express'); 
    var app = express(); 
    var bodyParser = require('body-parser');
    var multer = require('multer');
    var fs = require('fs');
    var path = require('path');
    var zip = require('express-zip');

    app.use(function(req, res, next) { //allow cross origin requests
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
        res.header("Access-Control-Allow-Origin", "http://localhost");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    /** Serving from the same express Server
    No cors required */
    app.use(express.static('../client'));
    app.use(bodyParser.json());  

    var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, './uploads/');
        },
        filename: function (req, file, cb) {
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
        }
    });

    var upload = multer({ //multer settings
                    storage: storage
                }).array('files', 10);

    /** API path that will upload the files */
    app.post('/upload', function(req, res) {
        upload(req,res,function(err){
            if(err){
                 res.json({error_code:1,err_desc:err});
                 return;
            }
            var responseObj = [];
            for(var i=0; i < req.files.length; i++){
                responseObj.push({
                    name:req.files[i].originalname,
                    path:req.files[i].filename,
                    size:req.files[i].size
                });
            }
             res.json(responseObj);
        });
    });

    app.get('/api/files/download/:id', function(req, res) {
        var filePath = './uploads/' + req.params.id;
        var stat = fs.statSync(filePath);      
        var fileToSend = fs.readFileSync(filePath);      
        res.writeHead(200, {
            'Content-Type': 'application/pdf',
            'Content-Length': stat.size,
            'Content-Disposition': req.params.id
        });
        res.end(fileToSend);
      });

    app.get('/api/files/downloadAll', function(req, res) {
        res.zip([
                    { name: 'files-1505502650316.pdf', path: path.join(__dirname, 'uploads', 'files-1505502650316.pdf')},
                    { name: 'files-1505502650348.pdf', path: path.join(__dirname, 'uploads', 'files-1505502650348.pdf')},
                    { name: 'files-1505502650363.pdf', path: path.join(__dirname, 'uploads', 'files-1505502650363.pdf')},
                    { name: 'files-1506018801916.pdf', path: path.join(__dirname, 'uploads', 'files-1506018801916.pdf')}
                ]);
    });

    app.listen('3000', function(){
        console.log('running on 3000...');
    });