angular.module('fileUpload', ['ngFileUpload', 'ngFileSaver'])
.controller('MyCtrl',['Upload','$window', '$http','FileSaver', 'Blob', function(Upload,$window, $http, FileSaver, Blob){
    var vm = this;
    vm.uploadedFile = [];
    vm.submit = function(){ //function to call on form submit
        if (vm.upload_form.files.$valid && vm.files) { //check if from is valid
            vm.upload(vm.files); //call upload function
        }
    }
    
    vm.upload = function (files) {
        Upload.upload({
            url: 'http://localhost:3000/upload', //webAPI exposed to upload the file
            arrayKey: '',
            data:{files:files} //pass file as data, should be user ng-model
        }).then(function (resp) { //upload function returns a promise
            if(resp.data.error_code === 0){ //validate success
                $window.alert('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
            } else {
                vm.uploadedFile = resp.data;
            }
        }, function (resp) { //catch error
            console.log('Error status: ' + resp.status);
            $window.alert('Error status: ' + resp.status);
        }, function (evt) { 
            console.log(evt);
            //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            //vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
        });
    };

    vm.download = function(path) {
        var request = {			    
            url: 'http://localhost:3000/api/files/download/' + path,
            method: "GET",
            headers: {
               'Content-type': 'application/json'
            },
            responseType: 'arraybuffer'
        };
        $http(request)
        .then(function (data, status, headers) {
            var blob = new Blob([data.data], { type: data.headers('Content-Type') });
            var fileName = data.headers('content-disposition');
            FileSaver.saveAs(blob, fileName);           
        });
      };

      vm.downloadAll = function() {
        var request = {			    
            url: 'http://localhost:3000/api/files/downloadAll/',
            method: "GET",
            responseType: 'arraybuffer'
        };
        $http(request)
        .then(function (data, status, headers) {
            var blob = new Blob([data.data], { type: data.headers('Content-Type') });
            var fileName = data.headers('Content-Disposition');
            FileSaver.saveAs(blob, fileName);           
        });
      }
}]);